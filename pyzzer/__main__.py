if __name__ == '__main__':
    import sys, pyzzer
    try:
        rc = pyzzer.main()
    except Exception as e:
        s = str(e) or type(e).__name__
        print('Failed: %s' % s)
        rc = 9
    sys.exit(rc)
