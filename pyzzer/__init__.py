# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2014 Vinay M. Sajip. See LICENSE for licensing information.
#
'''
Usage: pyzzer [options] DIR_OR_FILE_OR_ARCHIVE [DIR_OR_FILE ...]

Convert Python source directories and files to runnable zip files. The first
argument can be an existing archive, which can be added to with additional
sources.

Options:
  --version       show program's version number and exit
  -h, --help      show this help message and exit
  -s SHEBANG      Specify a shebang line to prepend to the archive, or the
                  path to an interpreter which can be used to determine the
                  shebang line to use. Defaults to "#! /usr/bin/env python".
  -m MODULE:ATTR  Specify a callable which is the main entry point.
  -x REGEX        Specify regexes to exclude from the zip  (can specify more
                  than once).
  -o FILENAME     Specify the path of the file to write to. If not specified
                  and a source archive is given, it will be used; otherwise
                  the extension defaults to .pyz and the name defaults to the
                  first directory or file specified.
  -v              Provide information about progress.
  -i              Inspect an existing archive.
  -l LAUNCHER     Specify a Windows launcher to use (t32/w32/t64/w64).
  -r              Recurse package directories.
'''

from io import BytesIO
import optparse # support 2.6
import os
import re
import stat
import struct
import subprocess
import sys
import zipfile

try:
    from . import launchers
except ImportError:
    launchers = None

__version__ = '0.1.2'

MAIN_TEMPLATE = '''if __name__ == '__main__':
    import sys

    def _resolve(module, func):
        __import__(module)
        mod = sys.modules[module]
        parts = func.split('.')
        result = getattr(mod, parts.pop(0))
        for p in parts:
            result = getattr(result, p)
        return result

    try:
        func = _resolve('%s', '%s')
        rc = func() # None interpreted as 0
    except Exception as e:
        sys.stderr.write('%%s\\n' %% e)
        raise
        rc = 1
    sys.exit(rc)
'''

CALLABLE_RE = re.compile('(?P<mod>\w+(\.\w+)*):(?P<func>\w+(\.\w+)*)')

DEFAULT_SHEBANG = '#! /usr/bin/env python'

#def _callback(option, opt_str, value, parser):
#    result = ''
#    if parser.rargs:
#        arg = parser.rargs[0]
#        if not arg or arg[0] != '-':
#            result = parser.rargs.pop(0)
#    setattr(parser.values, option.dest, result)

#def _collect_interpreters(s):
#    candidates = [c.strip() for c in s.split(',') if c.strip()]
#    result = []
#    for c in candidates:
#        cmd = [c, '-c', "import sys; print(sys.executable)"]
#        try:
#            p = subprocess.Popen(cmd, stdout=subprocess.PIPE,
#                                 stderr=subprocess.STDOUT, shell=False)
#            out, err = p.communicate()
#            if p.returncode:
#                raise ValueError('interpreter returned non-zero return '
#                                 'code %d' % p.returncode)
#            else:
#                s = out.strip()
#                if s != sys.executable:
#                    result.append(s)
#        except Exception as e:
#            print('Couldn\'t find interpreter %r: %s' % (c, e))
#    return result

#def _compile(interpreter, arg, is_package):
#    if is_package:
#        cmd = [interpreter, '-m', 'compileall', '-qf', arg]
#    elif os.path.isdir(arg):
#        cmd = [interpreter, '-m', 'compileall', '-qlf', arg]
#    else:
#        cmd = [interpreter, '-m', 'compileall', '-qf', arg]
#    p = subprocess.Popen(cmd, shell=False)
#    p.wait()

def _examine_possible_archive(s):
    launcher = shebang = data = None
    try:
        with open(s, 'rb') as f:
            all_data = f.read()
        pos = all_data.rfind(b'PK\x05\x06')
        if pos >= 0:
            end_cdr = all_data[pos + 12:pos + 20]
            cdr_size, cdr_offset = struct.unpack('<LL', end_cdr)
            arc_pos = pos - cdr_size - cdr_offset
            data = all_data[arc_pos:]
            if arc_pos > 0:
                pos = all_data.rfind(b'#!', 0, arc_pos)
                if pos >= 0:
                    shebang = all_data[pos:arc_pos]
                    if pos > 0:
                        launcher = all_data[:pos]
    except Exception:
        pass
    return launcher, shebang, data

def feedback(rp, path):
    print(rp)

def main():
    parser = optparse.OptionParser(usage='%prog [options] [DIR_OR_FILE_OR_'
                                         'ARCHIVE] [DIR_OR_FILE ...]\n\n'
                                         'Convert Python source directories '
                                         'and files to runnable zip files. '
                                         'The first argument can be an '
                                         'existing archive, which can be '
                                         'added to with additional sources.',
                                         version='%%prog %s' % __version__,
                                         prog=__name__)
    parser.add_option('-s', dest='shebang', metavar='SHEBANG',
                      help='Specify a shebang line to prepend to the archive, '
                           'or the path to an interpreter which can be used '
                           'to determine the shebang line to use. '
                           'Defaults to "#! /usr/bin/env python" on POSIX and '
                           'to the value of sys.executable on Windows.')
    parser.add_option('-m', dest='main', metavar='MODULE:ATTR',
                      help='Specify a callable which is the main entry point.')
    parser.add_option('-x', dest='exclude', metavar='REGEX',
                      action='append',
                      help='Specify regexes to exclude from the zip '
                      ' (can specify more than once).')
    parser.add_option('-o', dest='output', metavar='FILENAME',
                      help='Specify the path of the file to write to. If not '
                      'specified and a source archive is given, it will be '
                      'used; otherwise the extension defaults to .pyz and the '
                      'name defaults to the first directory or file '
                      'specified.')
    parser.add_option('-v', dest='verbose', default=False,
                      action='store_true',
                      help='Provide information about progress.')
    parser.add_option('-i', dest='inspect', default=False,
                      action='store_true',
                      help='Inspect an existing archive.')
    if launchers:
        parser.add_option('-l', dest='launcher',
                          choices=('t32', 'w32', 't64', 'w64'),
                          help='Specify a Windows launcher to use '
                               '(t32/w32/t64/w64).')
#    parser.add_option('-c', dest='compile', metavar='INTERPRETERS',
#                      action='callback', callback=_callback,
#                      help='Byte-compile files using specified interpreters '
#                           '(comma-separated).')
    parser.add_option('-r', dest='recurse', default=False,
                      action='store_true',
                      help='Recurse package directories.')
    options, args = parser.parse_args()
    if not args and not options.main:
        parser.print_help()
        return 1
    if options.main:
        m = CALLABLE_RE.match(options.main)
        if not m:
            raise ValueError('Malformed -m: %s' % options.main)
        d = m.groupdict()
        module, func = d['mod'], d['func']
    if options.output:
        output = options.output
    else:
        output = args[0]

    if not options.exclude:
        excluded = []
    else:
        excluded = [re.compile(e) for e in options.exclude]

#    if options.compile is None:
#        interpreters = []
#    else:
#        # -c specified
#        interpreters = [sys.executable]
#        if options.compile: # some value specified for -c
#            interpreters.extend(_collect_interpreters(options.compile))

    output_name, output_ext = os.path.splitext(output)

    launcher = shebang = data = None
    if args:
        arg = args[0]
        if os.path.isfile(arg):
            launcher, shebang, data = _examine_possible_archive(arg)
    if data:    # there is an archive
        args.pop(0)
        mode = 'a'
    else:
        data = b''
        mode = 'w'
    stream = BytesIO(data)
    # ZipFile is not a context manager in 2.6.
    zf = zipfile.ZipFile(stream, mode, zipfile.ZIP_DEFLATED)
    try:
        if options.inspect:
            if launcher:
                print('There is a launcher.')
            if shebang:
                print('Shebang: %s' % shebang.decode('utf-8'))
            print('Archive contents:')
            for zi in zf.infolist():
                print('  %s' % zi.filename)
            return
        seen = set([zi.filename for zi in zf.infolist()])
        for arg in args:
            arg = os.path.abspath(arg)
            if not os.path.isdir(arg):
                # Handle files
                path = arg
                # If only one file is specified and there are no files
                # in the archive, add the file as __main__.py, unless -m
                # is specified.
                if not options.main and len(args) == 1 and not zf.infolist():
                    rp = '__main__.py'
                else:
                    rp = os.path.basename(path)
                if rp in seen:
                    raise ValueError('Already added to zip: %s', rp)
                seen.add(rp)
                zf.write(path, rp)
                if options.verbose:
                    feedback(rp, path)
            else:
                # Handle directories
                fn = os.path.join(arg, '__init__.py')
                if os.path.exists(fn):
                    base = os.path.dirname(arg)
                    is_package = True
                else:
                    base = arg
                    is_package = False

#                for interpreter in interpreters:
#                    _compile(interpreter, arg, is_package)

                for root, dirs, files in os.walk(arg):
                    if is_package:
                        if not options.recurse:
                            del dirs[:] # no recursing
                        else:
                            for vcsdir in ('.hg', '.git', '.svn', '.bzr'):
                                if vcsdir in dirs:
                                    dirs.remove(vcsdir)
                    else:
                        del dirs[:] # no recursing

                    for fn in files:
                        if fn in ('.hgignore', '.gitignore', '.bzrignore'):
                            continue
                        elif fn.endswith(('.pyc', '.pyo')):
                            continue
#                        elif not interpreters and fn.endswith(('.pyc', '.pyo')):
#                            continue
                        path = os.path.join(root, fn)
                        rp = os.path.relpath(path, base)
                        skip = False
                        for e in excluded:
                            if e.search(rp):
                                skip = True
                                break
                        if skip:
                            continue
                        if rp in seen:
                            raise ValueError('Already added to zip: %s', rp)
                        seen.add(rp)
                        zf.write(path, rp)
                        if options.verbose:
                            feedback(rp, path)
        if options.main:
            s = MAIN_TEMPLATE % (module, func)
            zf.writestr('__main__.py', s.encode('utf-8'))
            seen.add('__main__.py')
        if '__main__.py' not in seen:
            raise ValueError('No __main__.py and -m not specified.')
    finally:
        zf.close()

    if launchers and output_ext.lower() == '.exe' and not options.launcher:
        # If running on 64-bit Windows, assume that a 64-bit launcher
        # is wanted. Otherwise, 32-bit. The console launcher is most likely.
        if os.name == 'nt' and struct.calcsize('P') == 8:
            bits = 64
        else:
            bits = 32
        options.launcher = 't%d' % bits
        msg = ('A .exe specified for output, but no launcher selected: '
               'using %s.\n' % options.launcher)
        sys.stderr.write(msg)

    if launchers and options.launcher:
        launcher = getattr(launchers, options.launcher)
    if shebang is None or options.shebang:
        # If you're running on Windows, and a launcher is requested,
        # a better default shebang is sys.executable
        if not launchers or not options.launcher or os.name != 'nt':
            default = DEFAULT_SHEBANG
        else:
            default = '#! %s' % sys.executable
        shebang = os.linesep.join([options.shebang or default,
                                  '# A zipped Python application',
                                  '# Built with pyzzer', '']).encode('utf-8')
        if not shebang.startswith(b'#!'):
            shebang = b'#!' + shebang
    ofn = output_name + output_ext
    with open(ofn, 'wb') as f:
        if launcher:
            f.write(launcher)
        f.write(shebang)
        f.write(stream.getvalue())
    if os.name == 'posix':
        mode = os.stat(ofn).st_mode
        os.chmod(ofn, mode | stat.S_IXUSR)
