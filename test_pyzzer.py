# -*- coding: utf-8 -*-
#
# Copyright (C) 2013 Vinay M. Sajip. See LICENSE for licensing information.
#
# Test harness for pyzzer.
#
import os
import shutil
import subprocess
import sys
import tempfile
import unittest
import zipfile

import pyzzer
from pyzzer import launchers

class PyzzerTest(unittest.TestCase):
    def setUp(self):
        self.workdir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.workdir)

    if sys.version_info[:2] <= (2,6):
        def assertIn(self, substr, string):
            self.assertTrue(substr in string)

    def make_command(self, *args, **kwargs):
        variant = kwargs.get('variant', 'pyzzer.pyz')
        return [sys.executable, variant] + list(args)

    def run_command(self, cmd):
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        out, err = p.communicate()
        return p, out, err

    def get_files(self):
        main = os.path.join('tests', '__main__.py')
        f1 = os.path.join('tests', 'boz.py')
        d1 = os.path.join('tests', 'foo')
        d2 = os.path.join('tests', 'bar')
        return main, f1, d1, d2

    def get_contents(self, filename):
        zf = zipfile.ZipFile(filename, 'r')
        try:
            result = set([zi.filename for zi in zf.infolist()])
        finally:
            zf.close()
        return result

    def get_content_data(self, filename, arcname):
        zf = zipfile.ZipFile(filename, 'r')
        try:
            result = zf.open(arcname).read()
        finally:
            zf.close()
        return result

    def test_basic(self):
        dest = os.path.join(self.workdir, 'dummy')
        main, f1, d1, d2 = self.get_files()
        cmd = self.make_command('-o', dest, main, d1, f1, d2)
        p, out, err = self.run_command(cmd)
        self.assertEqual(p.returncode, 0)
        self.assertEqual(out, b'')
        self.assertEqual(err, b'')
        actual = self.get_contents(dest)
        expected = set(['__main__.py', 'foo/__init__.py', 'baz.py',
                        'foo/bar.py', 'foo.py', 'boz.py'])
        self.assertEqual(actual, expected)
        with open(dest, 'rb') as f:
            data = f.read()
        self.assertIn(b'/usr/bin/env python', data)

    def test_wrapped_main(self):
        dest = os.path.join(self.workdir, 'dummy')
        main, f1, d1, d2 = self.get_files()
        cmd = self.make_command('-o', dest, '-m', 'foo:main', d1, f1, d2)
        p, out, err = self.run_command(cmd)
        self.assertEqual(p.returncode, 0)
        self.assertEqual(out, b'')
        self.assertEqual(err, b'')
        actual = self.get_contents(dest)
        expected = set(['__main__.py', 'foo/__init__.py', 'baz.py',
                        'foo/bar.py', 'foo.py', 'boz.py'])
        self.assertEqual(actual, expected)
        with open(dest, 'rb') as f:
            data = f.read()
        self.assertIn(b'/usr/bin/env python', data)

    def test_no_main(self):
        dest = os.path.join(self.workdir, 'dummy')
        main, f1, d1, d2 = self.get_files()
        cmd = self.make_command('-o', dest, d1, f1, d2)
        p, out, err = self.run_command(cmd)
        self.assertEqual(p.returncode, 1)
        self.assertEqual(out, b'')
        self.assertIn(b'ValueError: No __main__.py and -m not specified.', err)

    def test_wrong_variant(self):
        dest = os.path.join(self.workdir, 'dummy')
        main, f1, d1, d2 = self.get_files()
        cmd = self.make_command('-o', dest, '-l', 't64', main, d1, f1, d2,
                                variant='pyzzerw.pyz')
        # test with the correct variant first
        p, out, err = self.run_command(cmd)
        self.assertEqual(p.returncode, 0)
        self.assertEqual(out, b'')
        self.assertEqual(err, b'')
        actual = self.get_contents(dest)
        expected = set(['__main__.py', 'foo/__init__.py', 'baz.py',
                        'foo/bar.py', 'foo.py', 'boz.py'])
        self.assertEqual(actual, expected)
        with open(dest, 'rb') as f:
            data = f.read()
        if os.name == 'posix':
            self.assertIn(b'/usr/bin/env python', data)
        else:
            self.assertIn(sys.executable.encode('utf-8'), data)
        # and now the variant without launchers
        cmd = self.make_command('-o', dest, '-l', 't64', main, d1, f1, d2)
        # test with the correct variant first
        p, out, err = self.run_command(cmd)
        self.assertEqual(p.returncode, 2)
        self.assertEqual(out, b'')
        self.assertIn(b'error: no such option: -l', err)

    def test_launchers(self):
        dest = os.path.join(self.workdir, 'dummy')
        main, f1, d1, d2 = self.get_files()
        for launcher in ('t32', 'w32', 't64', 'w64'):
            cmd = self.make_command('-o', dest, '-l', launcher,
                                    main, d1, f1, d2, variant='pyzzerw.pyz')
            p, out, err = self.run_command(cmd)
            self.assertEqual(p.returncode, 0)
            self.assertEqual(out, b'')
            self.assertEqual(err, b'')
            actual = self.get_contents(dest)
            expected = set(['__main__.py', 'foo/__init__.py', 'baz.py',
                            'foo/bar.py', 'foo.py', 'boz.py'])
            self.assertEqual(actual, expected)
            with open(dest, 'rb') as f:
                data = f.read()
            self.assertTrue(data.startswith(getattr(launchers, launcher)))
            if os.name == 'posix':
                self.assertIn(b'/usr/bin/env python', data)
            else:
                self.assertIn(sys.executable.encode('utf-8'), data)
            os.remove(dest)

    def test_implicit_launcher(self):
        dest = os.path.join(self.workdir, 'dummy.exe')
        main, f1, d1, d2 = self.get_files()
        cmd = self.make_command('-o', dest,
                                main, d1, f1, d2, variant='pyzzerw.pyz')
        p, out, err = self.run_command(cmd)
        self.assertEqual(p.returncode, 0)
        self.assertEqual(out, b'')
        self.assertIn(b'no launcher selected:', err)

    def test_shebang(self):
        dest = os.path.join(self.workdir, 'dummy')
        main, f1, d1, d2 = self.get_files()
        for shebang in ('frobozz', '#!frobozz'):
            cmd = self.make_command('-o', dest, '-s', shebang,
                                    main, d1, f1, d2)
            p, out, err = self.run_command(cmd)
            self.assertEqual(p.returncode, 0)
            self.assertEqual(out, b'')
            self.assertEqual(err, b'')
            actual = self.get_contents(dest)
            expected = set(['__main__.py', 'foo/__init__.py', 'baz.py',
                            'foo/bar.py', 'foo.py', 'boz.py'])
            self.assertEqual(actual, expected)
            with open(dest, 'rb') as f:
                data = f.read()
            self.assertIn(b'#!frobozz', data)

    def test_recursion(self):
        dest = os.path.join(self.workdir, 'dummy')
        main, f1, d1, d2 = self.get_files()
        cmd = self.make_command('-o', dest, '-r', main, d1, f1, d2)
        p, out, err = self.run_command(cmd)
        self.assertEqual(p.returncode, 0)
        self.assertEqual(out, b'')
        self.assertEqual(err, b'')
        actual = self.get_contents(dest)
        expected = set(['__main__.py', 'foo/__init__.py', 'baz.py',
                        'foo/bar.py', 'foo.py', 'boz.py',
                        'foo/subfoo/__init__.py',
                        'foo/subfoo/subbar.py'])
        self.assertEqual(actual, expected)
        with open(dest, 'rb') as f:
            data = f.read()
        self.assertIn(b'/usr/bin/env python', data)

    def test_exclusion(self):
        dest = os.path.join(self.workdir, 'dummy')
        main, f1, d1, d2 = self.get_files()
        cmd = self.make_command('-o', dest, '-r', '-x', 'bar',
                                main, d1, f1, d2)
        p, out, err = self.run_command(cmd)
        self.assertEqual(p.returncode, 0)
        self.assertEqual(out, b'')
        self.assertEqual(err, b'')
        actual = self.get_contents(dest)
        expected = set(['__main__.py', 'foo/__init__.py', 'baz.py',
                        'foo.py', 'boz.py', 'foo/subfoo/__init__.py'])
        self.assertEqual(actual, expected)
        with open(dest, 'rb') as f:
            data = f.read()
        self.assertIn(b'/usr/bin/env python', data)

    def test_progress(self):
        dest = os.path.join(self.workdir, 'dummy')
        main, f1, d1, d2 = self.get_files()
        cmd = self.make_command('-o', dest, '-v', main, d1, f1, d2)
        p, out, err = self.run_command(cmd)
        self.assertEqual(p.returncode, 0)
        actual = set(out.decode('utf-8').splitlines())
        self.assertEqual(actual, set(['__main__.py', 'foo.py', 'boz.py',
                                      'foo%s__init__.py' % os.sep, 'baz.py',
                                      'foo%sbar.py' % os.sep]))
        self.assertEqual(err, b'')
        actual = self.get_contents(dest)
        expected = set(['__main__.py', 'foo/__init__.py', 'baz.py',
                        'foo/bar.py', 'foo.py', 'boz.py'])
        self.assertEqual(actual, expected)
        with open(dest, 'rb') as f:
            data = f.read()
        self.assertIn(b'/usr/bin/env python', data)

    def test_implicit_main(self):
        dest = os.path.join(self.workdir, 'dummy')
        main, f1, d1, d2 = self.get_files()
        cmd = self.make_command('-o', dest, f1)
        p, out, err = self.run_command(cmd)
        self.assertEqual(p.returncode, 0)
        self.assertEqual(out, b'')
        self.assertEqual(err, b'')
        actual = self.get_contents(dest)
        expected = set(['__main__.py'])
        self.assertEqual(actual, expected)

    def test_no_args_with_main(self):
        dest = os.path.join(self.workdir, 'dummy')
        cmd = self.make_command('-o', dest, '-m', 'foo:main')
        p, out, err = self.run_command(cmd)
        actual = self.get_contents(dest)
        expected = set(['__main__.py'])
        self.assertEqual(actual, expected)
        data = self.get_content_data(dest, '__main__.py')
        self.assertIn(b"func = _resolve('foo', 'main')", data)

    def test_no_args_with_no_main(self):
        dest = os.path.join(self.workdir, 'dummy')
        cmd = self.make_command('-o', dest)
        p, out, err = self.run_command(cmd)
        self.assertEqual(p.returncode, 1)
        self.assertIn(b'Usage: pyzzer [options]', out)
        self.assertEqual(err, b'')

    def test_inspect(self):
        dest = os.path.join(self.workdir, 'dummy')
        main, f1, d1, d2 = self.get_files()
        cmd = self.make_command('-o', dest, f1)
        p, out, err = self.run_command(cmd)
        cmd = self.make_command('-i', dest)
        p, out, err = self.run_command(cmd)
        expected = (b'Shebang: #! /usr/bin/env python\n# A zipped Python '
                    b'application\n# Built with pyzzer\n\nArchive contents:\n'
                    b'  __main__.py\n')
        self.assertEqual(out, expected)

if __name__ == '__main__':
    unittest.main()
